Name: perl-Filter
Epoch: 2
Version: 1.64
Release: 2
Summary: Perl source filters
License: GPL-1.0-or-later OR Artistic-1.0-Perl
URL: https://metacpan.org/release/Filter
Source0: https://cpan.metacpan.org/authors/id/R/RU/RURBAN/Filter-%{version}.tar.gz
BuildRequires: coreutils findutils gcc make perl-devel perl-generators
BuildRequires: perl-interpreter perl(Config)  perl(Getopt::Long) perl(strict)
BuildRequires: perl(Carp) perl(Exporter) perl(warnings) perl(XSLoader)
BuildRequires: perl(Cwd) perl(FindBin) perl(lib) sed
BuildRequires: perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires: perl(Test::More) >= 0.88
BuildRequires: perl(:VERSION) >= 5.6

Requires: perl(Carp)
Suggests: bash gcc m4

%description
Source filters alter the program text of a module before Perl sees it, much as
a C preprocessor alters the source text of a C program before the compiler
sees it.

%package_help

%prep
%setup -q -n Filter-%{version}
find examples -type f -exec chmod -x -- {} +
sed -i -e '1 s|.*|%(perl -MConfig -e 'print $Config{startperl}')|' \
    examples/filtdef

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1 OPTIMIZE="$RPM_OPT_FLAGS"
%make_build

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test

%files
%{perl_vendorarch}/auto/*
%{perl_vendorarch}/Filter*
%doc examples Changes README

%files help
%{_mandir}/man3/*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 2:1.64-2
- drop useless perl(:MODULE_COMPAT) requirement

* Sat Oct 22 2022 dongyuzhen <dongyuzhen@h-partners.com> - 2:1.64-1
- upgrade version to 1.64

* Fri Jan 14 2022 tianwei <tianwei12@huawei.com> - 2:1.60-2
- fix perl version required symbol

* Thu Jan 28 2021 liudabo <liudabo1@huawei.com> - 2:1.60-1
- upgrade version to 1.60

* Wed Sep 18 2019 openEuler Buildteam <buildteam@openeuler.org> - 2:1.59-1
- Package init
